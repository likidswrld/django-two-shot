# Generated by Django 4.2.1 on 2023-06-01 23:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0002_alter_receipt_vendor"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="account",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="account_receipts",
                to="receipts.account",
            ),
        ),
    ]
