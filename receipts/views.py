from django.shortcuts import render, get_object_or_404
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def show_receipt(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_object": receipt}
    return render(request, "receipts/list.html", context)
