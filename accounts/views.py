from django.shortcuts import render, redirect
from accounts.forms import LogInForm
from django.contrib.auth import authenticate, login as auth_login, logout
from django.contrib.auth.models import User
from receipts.models import Receipt


# Create your views here.
def login(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                auth_login(request, user)
                return redirect("home")
    else:
        form = LogInForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def my_receipts(request):
    receipts = Receipt.objects.filter(author=request.user)
    context = {
        "receipts_objects": receipts,
    }


def logout(request):
    logout(request)
    return redirect("home")
